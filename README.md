By Andre Araujo, July 30 2014

This was adapted from dotfiles.git, by the Startup Engineering class.
I have done some modifications (and might do more in the future), that's why I committed my own version.

To adequately configure the programming environment in the new server, do the following:

```sh
cd $HOME
git clone https://andrefaraujo@bitbucket.org/andrefaraujo/dotfiles.git
ln -sb dotfiles/.screenrc .
ln -sb dotfiles/.bash_profile .
ln -sb dotfiles/.bashrc .
ln -sb dotfiles/.bashrc_custom .
mv .emacs.d .emacs.d~
ln -s dotfiles/.emacs.d .
```

- Make sure there are no previously-existing ".emacs" file in the home folder.
- Make sure el-get installs fine the first time you use emacs; if it does not work, try the lazy-install method from: https://github.com/dimitri/el-get#the-lazy-installer
- To download "lua-mode" for Lua interpreting on emacs, do: "<M-x> el-get-install lua-mode"

See also http://github.com/startup-class/setup to install prerequisite
programs. If all goes well, in addition to a more useful prompt, now you can
do `emacs -nw hello.js` and hitting `C-c!` to launch an interactive SSJS
REPL, among many other features. See the
[Startup Engineering Video Lectures 4a/4b](https://class.coursera.org/startup-001/lecture/index)
for more details.

LICENSE: MIT